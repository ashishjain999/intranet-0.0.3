<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script>
        let globalUserId = {!! Auth::guest() ?:auth()->user()->id !!};
    </script>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = {csrfToken: '{{csrf_token()}}'} </script>

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{asset('css/pnotify.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/pnotify.brighttheme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my/custom.css') }}" rel="stylesheet">

</head>
<body>
<div id="app">
    {{--@include('layouts.header')--}}
    <navigation></navigation>
    <div class="page-header">
        <div class="container">
            <!--Router-->
            <router-view></router-view>

            <!-- set progressbar -->
            <vue-progress-bar></vue-progress-bar>

            <!--Footer-->
            <footer id="footer">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="list-unstyled">
                            <li class="float-lg-right"><a href="#top">Back to top</a></li>
                            <li><a href="https://cactus.com">Make by Cactus</a></li>
                        </ul>
                    </div>
                </div>

            </footer>
        </div>
    </div>
</div>

{{-- Load the application scripts --}}
@if (app()->isLocal())
    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
@else
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endif
</body>
</html>
