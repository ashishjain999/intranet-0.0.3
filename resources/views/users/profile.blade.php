@extends('layouts.app')
@push('scripts')
    <!--User script-->
    <script src="{{ asset('js/my/user.js') }}"></script>
@endpush
@section('content')
    <div>
        @if(Session::has('alert'))
            <p class="alert alert-dismissible alert-success">{{ Session::get('alert') }}
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </p>
        @endif
    </div> <!-- end flash-message -->
    <form role="form" enctype="multipart/form-data" method="post" id="form_post"
          action="{{'/users/'.$user->id}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="row my-2">
            <div class="col-lg-8 order-lg-2">
                <h3><u>ABOUT ME</u></h3>
                <div class="form-group">
                    <input name="about_me" type="text" class="form-control" id="about_me"
                           placeholder="Please add it."
                           value="{{$user->about_me}}">
                </div>
                <h3><u>CONTACT INFORMATION</u></h3>
                <div class="form-group">
                    <label>Email address</label>
                    <label class="form-control">{{$user->email}}</label>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                        anyone else.
                    </small>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <label class="form-control">{{$user->name}}</label>
                </div>
                <div class="form-group">
                    <label for="team">Team</label>
                    <select name="team" class="form-control">
                        <option value="{{null}}">Select</option>
                        @foreach(config('config.team') as $key => $team)
                            <option value="{{$key}}" {{$key == $user->team ? 'selected' : ''}}>{{$team}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="extno">Extn.No.</label>
                    <input name="extno" type="number" class="form-control" id="extno"
                           placeholder="Extension Number"
                           value="{{$user->extn_no}}">
                </div>
                <div class="form-group">
                    <label for="designation">Designation</label>
                    <input name="designation" type="text" class="form-control" id="designation"
                           placeholder="Designation"
                           value="{{$user->designation}}">
                </div>
                <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input name="mobile" type="number" class="form-control" id="mobile"
                           placeholder="Mobile"
                           value="{{$user->mobile}}">
                </div>
                <div class="form-group">
                    <label for="location">Location</label>
                    <input name="location" type="text" class="form-control" id="location"
                           placeholder="Location"
                           value="{{$user->location}}">
                </div>
                <div class="form-group">
                    <label for="birthday">Birthday</label>
                    <input name="birthday" type="text" class="form-control" id="birthday"
                           placeholder="Birthday"
                           value="{{$user->birthday}}">
                </div>
                <div class="form-group row">

                    <div class="col-lg-9">
                        <input type="submit" class="btn btn-primary btn-sm" value="Save Changes">
                    </div>
                </div>

            </div>
            <div class="col-lg-4 order-lg-1">
                <div class="text-center">
                    <img src="{{$user->file}}" class="mx-auto img-fluid img-circle d-block" alt="avatar">
                    <input type="file" class="form-control" name="image_file"
                           accept="image/gif, image/jpeg, image/png"/>
                </div>

                <!-- Unpublished_posts -->
                <div class="unpublished-posts">
                    <br/>
                    <a class="btn btn-outline-secondary btn-sm" id="create_initial_post" href="{{url('posts')}}">
                        Create a post
                    </a>
                    <h6><u>My Unpublished Posts</u></h6>
                    <ul class="unpublished-posts-listing">
                        @foreach($unpublished as $post)
                            <li class="items">
                                <a href='{{url('/posts/'.$post->id.'/edit')}}'>{{$post->title}}</a><br/>
                                <?php $text = strip_tags($post->text) ?>
                                @if(strlen($text) > 50)
                                    <?php echo substr($text, 0, strpos($text, ' ', 50)) . '...' ?>
                                @else
                                    <?php echo $text; ?>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="unpublished-posts">
                    <h6><u>My Published Posts</u></h6>
                    <ul class="unpublished-posts-listing">
                        @foreach($published as $post)
                            <li class="items">
                                <a href='{{url('/posts/'.$post->id.'/edit')}}'>{{$post->title}}</a><br/>
                                <?php $text = strip_tags($post->text) ?>
                                @if(strlen($text) > 50)
                                    {{ substr($text, 0, strpos($text, ' ', 50)) . '...'  }}
                                @else
                                    {{ $text  }}
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </form>
@endsection