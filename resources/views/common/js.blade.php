<!-- JQuery & JQuery UI -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Bootstrap v4.1.1 -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Vue.js -->
{{--<script src="{{ asset('js/app.js') }}"></script>--}}

{{--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>--}}

<!-- For notification -->
<script src="{{ asset('js/pnotify.js') }}"></script>


@stack('scripts')
<noscript>Your browser does not support JavaScript!</noscript>

