<!-- Create button -->
<button class="btn btn-primary btn-sm" id="create_initial_post">Create a post</button>
<button class="btn btn-primary btn-sm" id="close_initial_post" style="display: none;">Close</button>

<!-- Post an article form -->
<form enctype="multipart/form-data" class="form-post" id="form_post" style="display: none;">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" placeholder="Title" required>
    </div>
    <div class="form-group">
        <label for="post_body">What's on your mind?</label>
        <textarea class="form-control form-post-textarea" rows="3" id="post_body"
                  required></textarea>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox" value="true" id="published" checked>
            Publish this article
        </label>
    </div>
    <div class="form-group">
        <div class="pull-right">
            <button type="button" class="btn btn-primary btn-sm" id="save_post">Share</button>
        </div>
    </div>
</form>