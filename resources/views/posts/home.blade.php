@section('title', 'View all posts')
@extends('layouts.app')

@extends('posts.inside.scripts')

@section('content')
    <div class="row">
        <div class="col-sm-9">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @include('posts.inside.new-post')
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <span class="pinned-happening-title text-primary">{{count($data['data'])}} Posts</span>
            <ul class="posts">
                @foreach($data['data'] as $post)
                    <li class="post">
                        <div class="short-message">
                            <div class="row">
                                <div class="col-sm-2">
                                    <a href="{{url('/users/'.$post->user_id)}}">
                                        <img src="{{$post->photo}}" class="img-thumbnail">
                                    </a>
                                    <span class="text-center text-date"><strong>{{$post->updated_at_date}}</strong></span>
                                    <span class="text-center text-time"><strong>{{$post->updated_at_time}}</strong></span>
                                </div>
                                <div class="col-sm-10">
                                    <!-- Name of a person -->
                                    <h5 class="author-name">
                                        <a href="{{url('/users/'.$post->user_id)}}">{{$post->user_name}}</a> has
                                        posted an
                                        <span class="text-primary">article</span>
                                    </h5>
                                    <!-- Article -->
                                    <div class="article-content">
                                        <div class="post-title">
                                            <a href="{{url('/posts/'.$post->sef_url)}}"><strong>{{$post->title}}</strong></a>
                                        </div>
                                        <div class="article-content">
                                            <?php $text = strip_tags($post->text) ?>
                                            @if(strlen($text) > 50)
                                                {{ substr($text, 0, strpos($text, ' ', 50)) . '...'  }}
                                            @else
                                                {{ $text }}
                                            @endif
                                        </div>

                                    </div>
                                    <!-- Likes & Comments -->
                                    <div class="like-comment-count">
                                        <div class="posts-like">
                                            <div class="likes-count-outer-wrapper">
                                                @php $likeColor = ''; @endphp
                                                @if (isset($post->total_likes) && !empty($post->total_likes))
                                                    <?php
                                                    foreach ($post->total_likes as $likes) {
                                                        if ($likes[ 'post_id' ] === $post->id && $likes[ 'user_id' ] === Auth::id()) {
                                                            $likeColor = '';
                                                            break;
                                                        } else
                                                            $likeColor = '';
                                                    }
                                                    ?>
                                                @endif

                                                <a href="javascript:;"
                                                   class="like-post post_main_id_{{$post->id}}"
                                                   data-like-post-id="{{$post->id}}"
                                                   style="color: {{$likeColor}}">
                                                    Like
                                                </a>
                                                <span class="text-muted count data-count_{{$post->id}}"
                                                      data-like-count="">
                                                        ({{(isset($post->total_likes) && !empty($post->total_likes))
                                                            ?count($post->total_likes):'0'}})
                                                        </span>
                                            </div>
                                            <span>&nbsp;·&nbsp;</span>
                                            <div class="comments-count-outer-wrapper">
                                                <a href="javascript:;" class="text-muted">Comment</a>
                                                <span class="count text-muted comments_count_{{$post->id}}">
                                                        ({{(isset($post->total_comments) && !empty($post->total_comments))
                                                            ?count($post->total_comments):'0'}})
                                                    </span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Comments wrapper -->
                                    <div class="comment-like-wraper">
                                        <!-- Likes only -->
                                        <div class="posts-like">
                                            <a class="like-button" href="javascript:;">
                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                            </a>
                                            <div class="liked-this like_this_{{$post->id}}">
                                                @if (isset($post->total_likes) && !empty($post->total_likes))
                                                    <?php
                                                    $key = array_search(Auth::id(),
                                                        array_column((array)$post->total_likes, 'user_id'));
                                                    ?>
                                                    @if($key !== false)
                                                        <a class="text-primary" data-toggle="modal"
                                                           data-target="#postModal{{$post->id}}"
                                                           href="javascript:;">
                                                            You
                                                            @if(count($post->total_likes) > 1)
                                                                and {{count($post->total_likes)-1}} others
                                                            @endif
                                                        </a>
                                                    @else
                                                        <a class="text-primary" data-toggle="modal"
                                                           data-target="#postModal{{$post->id}}"
                                                           href="javascript:;">
                                                            {{$post->total_likes[0]['user_name']}}
                                                            @if(count($post->total_likes) > 1)
                                                                and {{count($post->total_likes)-1}} others
                                                            @endif
                                                        </a>
                                                    @endif
                                                    <span class="text-primary">like this</span>
                                                @endif
                                            </div>
                                            <!--Modal for likes-->
                                            <div class="modal fade" id="postModal{{$post->id}}"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Users
                                                                who liked this post.</h5>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body modal_body_{{$post->id}}">
                                                            @if (isset($post->total_likes) && !empty($post->total_likes))
                                                                <div class="row">
                                                                    @foreach($post->total_likes as $likes)
                                                                        <div class="col-sm-6">
                                                                            <a class="text-primary"
                                                                               href="{{url('users/'.$likes['user_id'])}}">
                                                                                <img src="{{$likes['user_photo']}}"
                                                                                     class="img-thumbnail img-modal">
                                                                                <span>{{$likes['user_name']}}</span>
                                                                            </a>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button"
                                                                    class="btn btn-secondary btn-sm"
                                                                    data-dismiss="modal">Okay
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- Comments only wrapper -->
                                        <div class="hidden-div">
                                            <div class="new_comment_{{$post->id}}"></div>
                                            @if(isset($post->total_comments) && count($post->total_comments))
                                                @foreach($post->total_comments as $comment)
                                                    <div class="comments-container comment-listing-wrapper comment_container_{{$comment['id']}}">
                                                        {{--<div class="load-more-comment">--}}
                                                        {{--<i class="fa fa-comment"></i>--}}
                                                        {{--<a>View more comments</a><!-- end ngIf: commentEditable -->--}}
                                                        {{--</div>--}}
                                                        <div class="comment-items">
                                                            <div class="comment">
                                                                <div class="post-image">
                                                                    <a href="javascript:;">
                                                                        <img src="{{$comment['comment_photo']}}">
                                                                    </a>
                                                                </div>

                                                                <span class="comment-author">
                                                                            <a href="javascript:;"
                                                                               class="liked-this">{{$comment['user_name']}}</a>
                                                                        </span>

                                                                <span class="comment-time-ago">{{$comment['created_at']}}</span>
                                                                <div class="comment-body-container">
                                                                    <div style="display: inline-block"
                                                                         class="comment-body comment_body_{{$comment['id']}}">{{$comment['text']}}</div>
                                                                    <span>
                                                                                @if(Auth::id() == $comment['user_id'] || request()->user()->hasRole('admin'))
                                                                            <a class="text-danger delete-comment comment_main_id_{{$comment['id']}}"
                                                                               href="javscript:;"
                                                                               data-delete-comment-id="{{$comment['id']}}"
                                                                               data-post-id="{{$post->id}}">
                                                                                        <i class="fa fa-trash-o"></i>
                                                                            </a>

                                                                            <a class="text-muted edit-comment comment_main_id_{{$comment['id']}}"
                                                                               data-toggle="modal"
                                                                               data-target="#editModal_{{$comment['id']}}"
                                                                               href="javascript:;">
                                                                                        <i class="fa fa-pencil"></i>
                                                                            </a>

                                                                        @endif
                                                                        <a class="like-comment comment-like-color comment_main_id_{{$comment['id']}}"
                                                                           data-like-comment-id="{{$comment['id']}}"
                                                                           data-post-id="{{$post->id}}"
                                                                           href="javascript:;">
                                                                                    <i class="fa fa-thumbs-up"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                    </span>

                                                                    <div class="liked-this-comment liked_this_comment_{{$comment['id']}}">
                                                                        @if (isset($comment['total_comments_likes']) && !empty($comment['total_comments_likes']))
                                                                            <?php
                                                                            $key = array_search(Auth::id(),
                                                                                array_column($comment[ 'total_comments_likes' ],
                                                                                    'user_id'));
                                                                            ?>
                                                                            @if($key !== false)
                                                                                <a class="text-primary"
                                                                                   data-toggle="modal"
                                                                                   data-target="#commentModal_{{$comment['id']}}"
                                                                                   href="javascript:;">
                                                                                    You
                                                                                    @if(count($comment['total_comments_likes']) > 1)
                                                                                        and {{count($comment['total_comments_likes'])-1}}
                                                                                        others
                                                                                    @endif
                                                                                </a>
                                                                            @else
                                                                                <a class="text-primary"
                                                                                   data-toggle="modal"
                                                                                   data-target="#commentModal_{{$comment['id']}}"
                                                                                   href="javascript:;">
                                                                                    {{$comment['total_comments_likes'][0]['user_name']}}
                                                                                    @if(count($comment['total_comments_likes']) > 1)
                                                                                        and {{count($comment['total_comments_likes'])-1}}
                                                                                        others
                                                                                    @endif
                                                                                </a>
                                                                            @endif

                                                                            <span class="text-primary">like this</span>
                                                                        @endif
                                                                    </div>

                                                                    <!--Modal for comment update-->
                                                                    <div class="modal edit-comment fade"
                                                                         id="editModal_{{$comment['id']}}">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title">
                                                                                        Edit a comment
                                                                                    </h5>
                                                                                    <button type="button"
                                                                                            class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body modal_edit_comment_{{$comment['id']}}">
                                                                                    <div class="form-group">
                                                                                        <div class="input-group mb-3">
                                                                                            <div class="input-group-prepend">
                                                                                                <div class="post-image">
                                                                                                    <a href="javascript:;">
                                                                                                        <img src="{{$comment['comment_photo']}}">
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>

                                                                                            <input type="text"
                                                                                                   id="edit_comment_{{$comment['id']}}"
                                                                                                   placeholder="Write a comment..."
                                                                                                   class="form-control"
                                                                                                   value="{{$comment['text']}}">
                                                                                            <div class="input-group-append">
                                                                                                <input type="button"
                                                                                                       value="Update"
                                                                                                       class="btn btn-default pull-right update_comment"
                                                                                                       data-comment-id="{{$comment['id']}}"
                                                                                                       data-dismiss="modal">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!--Modal for comments likes-->
                                                                    <div class="modal fade"
                                                                         id="commentModal_{{$comment['id']}}">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title"
                                                                                        id="commentModalLabel">
                                                                                        Users who liked this
                                                                                        comment.</h5>
                                                                                    <button type="button"
                                                                                            class="close"
                                                                                            data-dismiss="modal"
                                                                                            aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body modal_body_comment{{$comment['id']}}">
                                                                                    @if (isset($comment['total_comments_likes']) && !empty($comment['total_comments_likes']))
                                                                                        <div class="row">
                                                                                            @foreach($comment['total_comments_likes'] as $likes)
                                                                                                <div class="col-sm-6">
                                                                                                    <a class="text-primary"
                                                                                                       href="{{url('users/'.$likes['user_id'])}}">
                                                                                                        <img src="{{$likes['user_photo']}}"
                                                                                                             class="img-thumbnail img-modal">
                                                                                                        <span>{{$likes['user_name']}}</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button"
                                                                                            class="btn btn-secondary btn-sm"
                                                                                            data-dismiss="modal">
                                                                                        Okay
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>

                                        <!-- Write a comment -->
                                        <div class="comments-container comment-form">
                                            <form class="form-horizontal center-block">
                                                <input type="hidden" class="post_id" value="{{$post->id}}">
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <div class="post-image">
                                                                <img src="images/default-grav.jpg">
                                                            </div>
                                                        </div>

                                                        <textarea name="comment" cols="30" rows="1" required
                                                                  placeholder="Write a comment..."
                                                                  class="form-control comment-message-{{$post->id}}"></textarea>
                                                        <div class="input-group-append">
                                                            <input type="button" value="Comment"
                                                                   class="btn btn-default pull-right save_comment"
                                                                   data-post-id="{{$post->id}} ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            {{ $page->links()}}


        </div>
    </div>
@endsection
