@section('title', 'Login to Intranet')
@extends('layouts.app')

@section('content')
    <div class="login-register">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <h3 class="box-title m-b-20">Please Login</h3>
                    <div class="form-group has-error{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input type="text" name="email" placeholder="Email" class="form-control" required
                                   value="{{ old('email') }}" autofocus>
                            <span class="text-danger align-middle">
                            <span class="help-block">
                                @if ($errors->has('email'))
                                    <i class="fa fa-close"></i>
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </span>
                        </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" name="password" placeholder="Password" class="form-control" required
                                   placeholder="Password">
                            <span class="text-danger align-middle">
                            @if ($errors->has('password'))
                                    <i class="fa fa-close"></i>
                                    <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                @endif
                        </span>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button type="submit"
                                    class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light">Log
                                In
                            </button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Forgot your password?
                                <a href="javasctipt:;" class="text-primary m-l-5" onclick="new PNotify({
                                text: 'Please use your PC username and password.'
                            });">
                                    <strong>Reset here!</strong>
                                </a>
                            </p>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
