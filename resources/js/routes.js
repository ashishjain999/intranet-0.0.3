import VueRouter from 'vue-router';
import Login from './views/Login';
import Posts from './views/Posts';
import SinglePosts from './views/ViewPost';
import Home from './views/Home';
import Users from './views/Users';
import Error from './views/Error'
import store from './store/index';

let routes = [
    {
        path: '/posts',
        component: Posts,
        meta: {requiresAuth: true},
        props: { title: "View post" },
    },
    {
        path: '/posts/:post_slug',
        component: SinglePosts,
        meta: {requiresAuth: true},
    },
    {
        path: '/users/:id',
        component: Users,
        meta: {requiresAuth: true},
    },
    {
        path: '/home',
        component: Home,
        meta: {requiresAuth: true},
        props: { title: "This is home page" }
    },
    {
        path: '/login',
        component: Login,
        meta: {requiresAuth: false}
    },
    {
        path: '*',
        component: Error
    }
];

const router = new VueRouter({
    routes,
    mode: 'history',
    linkActiveClass: 'is-active',
    saveScrollPosition: true,

});

router.beforeEach((to, from, next) => {
    if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
        next('/login');
        return;
    }

    if (to.path === '/login' && store.state.isLoggedIn) {
        next('/home');
        return;
    }
    next();
});

export default router;

