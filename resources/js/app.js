/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import Vue from 'vue'; // Importing Vue Library
import VueRouter from 'vue-router'; // importing Vue router library
import router from './routes';
import Nav from './views/Navbar';
import VueProgressBar from 'vue-progressbar';

window.Vue = Vue;
Vue.use(VueRouter);

const options = {
    color: '#bffaf3',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'left',
    inverse: false
};

Vue.use(VueProgressBar, options);

//Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    components: {
        'navigation': Nav
    },
    router,
    axios,
});

// new Vue({
//     render: h => h(App),
//     router
// }).$mount('#app')