$(function () {

    $('#create_initial_post').on('click', function () {
        $('#form_post').show();
        $('#close_initial_post').show();
        $('#create_initial_post').hide();
    });

    $('#close_initial_post').on('click', function () {
        $('#form_post').hide();
        $('#close_initial_post').hide();
        $('#create_initial_post').show();
    });

    //array to store post
    var formData = {};
    $('#save_post').on('click', function (e) {

        if ($("#form_post")[0].checkValidity()) {
        }
        else {
            $("#form_post")[0].reportValidity();
            return false;
        }

        formData['post_title'] = $('#title').val();
        formData['post_body'] = CKEDITOR.instances.post_body.getData();
        formData['published'] = $('#published').prop('checked');

        $.ajax({
            method: 'POST',
            url: '/posts',
            data: formData,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                //console.log(response);
                location.reload();

            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //array to store comments
    var commentData = {};

    $('.save_comment').on('click', function () {
        commentData['comment'] = $('.comment-message-' + $(this).data('post-id')).val();
        commentData['post_id'] = $(this).data('post-id');
        $.ajax({
            method: 'POST',
            url: '/comments',
            data: commentData,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('body').alertPnotify({title: 'Comments', text: 'Comments was added successfully.', type: 'success'});

                $('.comments_count_' + response.post_id).text('(' + response.count + ')');
                var postId = '.new_comment_' + commentData['post_id'];
                //$(postId).empty();
                $('<div>', {class: 'comments-container comment-listing-wrapper comment_container_' + response.id}).appendTo($(postId));

                $('<div>', {class: 'comment-items'}).appendTo(postId + ' .comments-container');
                $('<div>', {class: 'comment'}).appendTo(postId + ' .comment-items');
                $('<div>', {class: 'post-image'}).appendTo(postId + ' .comment').append($('<a>', {}));
                $('<img>', {src: response.comment_photo}).appendTo($(postId + ' .post-image a'));
                $('<span>', {class: 'comment-author'}).appendTo(postId + ' .comment');
                $('<a>', {
                    text: response.user_name,
                    href: '',
                    class: 'liked-this'
                }).appendTo(postId + ' .comment-author');

                $('<span>', {
                    class: 'comment-time-ago',
                    text: ' ' + response.created_at
                }).appendTo(postId + ' .comment');
                var commentBody = $('<div>', {class: 'comment-body-container'}).appendTo(postId + ' .comment');
                $('<div>', {class: 'comment-body comment_body_' + response.id, style: "display: inline-block"})
                    .appendTo(commentBody).append(response.text);

                var spanBody = $('<span>').appendTo(commentBody);

                $('<a>', {
                    class: 'delete-comment comment_main_id_' + response.id, href: 'javascript:;',
                    'data-delete-comment-id': response.id,
                    'data-post-id': response.post_id,
                }).appendTo(spanBody).append(' ').append($('<i>', {class: 'fa fa-trash-o'}));

                $('<a>', {
                    class: 'text-muted edit-comment comment_main_id_' + response.id, href: 'javascript:;',
                    'data-toggle': 'modal', 'data-target': '#editModal_' + response.id
                }).appendTo(spanBody).append(' ').append($('<i>', {class: 'fa fa-pencil'}));

                $('<a>', {
                    class: 'like-comment comment-like-color comment_main_id_' + response.id,
                    text: '', 'data-like-comment-id': response.id, href: 'javascript:;'
                }).appendTo(spanBody).append(' ').append($('<i>', {class: 'fa fa-thumbs-up'}));

                //Edit a liked modal to be shown after editing a comment
                var modal = $('<div>', {class: 'modal fade', id: 'editModal_' + response.id}).appendTo(commentBody);
                var modalDialog = $('<div>', {class: 'modal-dialog'}).appendTo(modal);
                var modalContent = $('<div>', {class: 'modal-content'}).appendTo(modalDialog);

                //Header
                var modalHeader = $('<div>', {class: 'modal-header'}).appendTo(modalContent);
                $('<h5>', {class: 'modal-title', text: 'Edit a comment'}).appendTo(modalHeader);
                $('<button>', {class: 'close', type: 'button', 'data-dismiss': 'modal', 'aria-label': 'Close'})
                    .append($('<span>', {text: '×', 'aria-hidden': true})).appendTo(modalHeader);

                //Body
                var modalBody = $('<div>', {class: 'modal-body modal_edit_comment_' + response.id}).appendTo(modalContent);
                var formGroup = $('<div>', {class: 'form-group'}).appendTo(modalBody);
                var inputGroup = $('<div>', {class: 'input-group mb-3'}).appendTo(formGroup);

                var inputGroupPre = $('<div>', {class: 'input-group-prepend'}).appendTo(inputGroup);
                var postImage = $('<div>', {class: 'post-image'}).appendTo(inputGroupPre);
                var href = $('<a>', {href: 'javascript:;'}).appendTo(postImage);
                $('<img>', {src: response.comment_photo}).appendTo(href);

                $('<input>', {
                    type: 'text', id: 'edit_comment_' + response.id, class: 'form-control',
                    placeholder: 'Write a comment...', value: response.text
                }).appendTo(inputGroup);

                var inputGroupPost = $('<div>', {class: 'input-group-append'}).appendTo(inputGroup);
                $('<input>', {
                    type: 'button', value: 'Update', 'data-dismiss': 'modal',
                    'data-comment-id': response.id, class: 'btn btn-default pull-right update_comment'
                }).appendTo(inputGroupPost);

                // var span = $('<span>', {class: 'comment_main_id_' + response.id}).appendTo('.comment_body_' + response.id);
                // $('<a>', {
                //     class: 'text-muted edit-comment comment_main_id_' + response.id, href: 'javascript:;',
                //     'data-edit-comment-id': response.id,
                // }).appendTo(span).append(' ').append($('<i>', {class: 'fa fa-pencil'}));


                $('<div>', {class: 'liked-this-comment liked_this_comment_' + response.id}).appendTo(commentBody);
                $('.comment-message-' + commentData['post_id']).val('');

                $(postId).children().unwrap();
                $('<div>', {class: 'new_comment_' + commentData['post_id']}).appendTo('.hidden-div').prepend();


            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //Like button
    var likeData = {};
    $('.like-post').on('click', function () {
        //likeData['post_id'] = $(this).data('like-post-id');
        //console.log('post_id' +$(this).data('like-post-id'));
        $.ajax({
            method: 'POST',
            url: '/likes',
            data: {
                'post_id': $(this).data('like-post-id')
            },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.msg === 'deleted') {
                    $('body').alertPnotify({title: 'Post', text: 'You unlike the post', type: 'error'});

                    $('.post_main_id_' + response.post_id).css('color', '');
                    $('.data-count_' + response.post_id).text('(' + response.count + ')');
                    $('.like_this_' + response.post_id).empty();

                    if (response.users.length === 0) {
                        //nothing happened
                    } else {
                        if (response.single.length === 1) {
                            $('<a>', {
                                href: 'javascript:;', text: 'You', class: 'like-modal like_modal_' + response.post_id,
                                'data-toggle': 'modal', 'data-target': '#postModal' + response.post_id,
                                'data-like-modal': response.post_id,
                            }).appendTo('.like_this_' + response.post_id);
                            $('<span>', {text: ' like this'}).appendTo('.like_this_' + response.post_id);
                        } else {
                            $('<a>', {
                                href: 'javascript:;', class: 'like-modal like_modal_' + response.post_id,
                                'data-toggle': 'modal', 'data-target': '#postModal' + response.post_id,
                                'data-like-modal': response.post_id,
                                text: response.users[0].name + ' and ' + (response.users.length - 1) + ' others'
                            }).appendTo('.like_this_' + response.post_id);
                            $('<span>', {text: ' like this'}).appendTo('.like_this_' + response.post_id);
                        }
                    }

                } else if (response.msg === 'success') {
                    $('body').alertPnotify({title: 'Post', text: 'You like the post', type: 'success'});

                    $('.post_main_id_' + response.post_id).css('color', '');
                    $('.data-count_' + response.post_id).text('(' + response.count + ')');
                    $('.like_this_' + response.post_id).empty();

                    if (response.users.length === 1) {
                        $('<a>', {
                            href: 'javascript:;', text: 'You', class: 'like-modal like_modal_' + response.post_id,
                            'data-toggle': 'modal', 'data-target': '#postModal' + response.post_id,
                            'data-like-modal': response.post_id,
                        }).appendTo('.like_this_' + response.post_id);
                        $('<span>', {text: ' like this'}).appendTo('.like_this_' + response.post_id);
                    } else {
                        $('<a>', {
                            href: 'javascript:;', class: 'like-modal like_modal_' + response.post_id,
                            'data-toggle': 'modal', 'data-target': '#postModal' + response.post_id,
                            'data-like-modal': response.post_id,
                            text: 'You' + ' and ' + (response.users.length - 1) + ' others'
                        }).appendTo('.like_this_' + response.post_id);
                        $('<span>', {text: ' like this'}).appendTo('.like_this_' + response.post_id);
                    }
                }


            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //Call posts modal
    $(document.body).on('click', '.like-modal', function () {
        var main_post_id = $(this).data('like-modal');

        $.ajax({
            method: 'POST',
            url: '/get-post-users',
            data: {
                'post_id': $(this).data('like-modal')
            },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('.modal_body_' + main_post_id).empty();
                var row = $('<div>', {class: 'row'}).appendTo('.modal_body_' + main_post_id);
                $.each(response, function (index, value) {
                    var col = $('<div>', {class: 'col-sm-6'}).appendTo(row);
                    var link = $('<a>', {class: 'text-primary', href: '/users/' + value.id}).appendTo(col);
                    var image = (value.file == null) ? '/images/default-grav.jpg' : value.file;
                    $('<img>', {src: image, class: 'img-thumbnail img-modal'}).appendTo(link);
                    $('<span>', {text: value.name}).appendTo(link);
                });
            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //Comment like button
    $(document.body).on('click', '.like-comment', function () {
        //$('.like-comment').on('click', function () {
        console.log($(this).data('like-comment-id'));
        var postId = ($(this).data('post-id'));
        $.ajax({
            method: 'POST',
            url: '/comments-likes',
            data: {
                'comment_id': $(this).data('like-comment-id'),
                'post_id': postId
            },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);

                if (response.msg === 'deleted') {
                    $('body').alertPnotify({title: 'Comment', text: 'You unlike the comment', type: 'error'});

                    $('.comment_main_id_' + response.comment_id).css('color', '#b0aeaf');
                    //$('.data-count_' + response.count).text('(' + response.count + ')');
                    $('.liked_this_comment_' + response.comment_id).empty();

                    if (response.users.length === 0) {
                        //Nothing to be done
                    } else {
                        if (response.single.length === 1) {
                            $('<a>', {
                                href: 'javascript:;',
                                text: 'You',
                                'data-toggle': 'modal',
                                'data-comment-like-modal': response.comment_id,
                                'data-target': '#commentModal_' + response.comment_id,
                                class: 'comment-like-modal comment_like_modal_' + response.comment_id,
                            }).appendTo('.liked_this_comment_' + response.comment_id);
                            $('<span>', {
                                text: ' like this',
                                class: 'text-primary'
                            }).appendTo('.liked_this_comment_' + response.comment_id);
                        } else {
                            $('<a>', {
                                href: 'javascript:;',
                                'data-toggle': 'modal',
                                'data-comment-like-modal': response.comment_id,
                                'data-target': '#commentModal_' + response.comment_id,
                                class: 'comment-like-modal comment_like_modal_' + response.comment_id,
                                text: response.users[0].name + ' and ' + (response.users.length - 1) + ' others'
                            }).appendTo('.liked_this_comment_' + response.comment_id);
                            $('<span>', {
                                text: ' like this',
                                class: 'text-primary'
                            }).appendTo('.liked_this_comment_' + response.comment_id);
                        }
                    }
                } else if (response.msg === 'success') {
                    $('body').alertPnotify({title: 'Comment', text: 'You like the comment', type: 'success'});

                    $('.comment_main_id_' + response.comment_id).css('color', '#E95420');
                    //$('.data-count_' + response.post_id).text('(' + response.count + ')');
                    $('.liked_this_comment_' + response.comment_id).empty();

                    if (response.users.length === 1) {
                        $('<a>', {
                            href: 'javascript:;',
                            text: 'You',
                            class: 'comment-like-modal comment_like_modal_' + response.comment_id,
                            'data-toggle': 'modal',
                            'data-target': '#commentModal_' + response.comment_id,
                            'data-comment-like-modal': response.comment_id,
                        }).appendTo('.liked_this_comment_' + response.comment_id);
                        $('<span>', {
                            text: ' like this',
                            class: 'text-primary'
                        }).appendTo('.liked_this_comment_' + response.comment_id);
                    } else {
                        $('<a>', {
                            href: 'javascript:;', class: 'comment-like-modal comment_like_modal_' + response.comment_id,
                            'data-toggle': 'modal', 'data-target': '#commentModal_' + response.comment_id,
                            'data-comment-like-modal': response.comment_id,
                            text: 'You' + ' and ' + (response.users.length - 1) + ' others'
                        }).appendTo('.liked_this_comment_' + response.comment_id);
                        $('<span>', {
                            text: ' like this',
                            class: 'text-primary'
                        }).appendTo('.liked_this_comment_' + response.comment_id);
                    }
                }

                //Modal for viewing comments like users
                var modal = $('<div>', {class: 'modal fade', id: 'commentModal_' + response.comment_id})
                    .appendTo('.comment-body-container');

                var modelDialog = $('<div>', {class: 'modal-dialog'}).appendTo(modal);
                var modalContent = $('<div>', {class: 'modal-content'}).appendTo(modelDialog);

                //Header
                var modalHeader = $('<div>', {class: 'modal-header'}).appendTo(modalContent);
                $('<h5>', {class: 'modal-title', text: 'Users who liked this comment.'}).appendTo(modalHeader);
                $('<button>', {class: 'close', type: 'button', 'data-dismiss': 'modal', 'aria-label': 'Close'})
                    .append($('<span>', {text: '×', 'aria-hidden': true})).appendTo(modalHeader);

                //Body
                var modalBody = $('<div>', {class: 'mb modal-body modal_body_comment' + response.comment_id})
                    .appendTo(modalContent);

                //Footer
                var modalFooter = $('<div>', {class: 'modal-footer'}).appendTo(modalContent);
                $('<button>', {
                    class: 'btn btn-secondary btn-sm', type: 'button', 'data-dismiss': 'modal', text: 'Okay'
                }).appendTo(modalFooter);
            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //Call posts modal
    $(document.body).on('click', '.comment-like-modal', function () {
        var main_comment_id = $(this).data('comment-like-modal');

        $.ajax({
            method: 'POST',
            url: '/get-comment-users',
            data: {
                'comment_id': $(this).data('comment-like-modal')
            },
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('.modal_body_comment' + main_comment_id).empty();
                var row = $('<div>', {class: 'row'}).appendTo('.modal_body_comment' + main_comment_id);
                $.each(response, function (index, value) {
                    var col = $('<div>', {class: 'col-sm-6'}).appendTo(row);
                    var link = $('<a>', {class: 'text-primary', href: '/users/' + value.id}).appendTo(col);
                    var image = (value.file == null) ? '/images/default-grav.jpg' : value.file;
                    $('<img>', {src: image, class: 'img-thumbnail img-modal'}).appendTo(link);
                    $('<span>', {text: value.name}).appendTo(link);
                });
            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

    //Deleting comment
    $('body').on('click', '.delete-comment', function () {
        var box = confirm('Are you sure you want to delete a comment?');
        if (box == true) {
            var deleteId = $(this).data('delete-comment-id');
            var postId = $(this).data('post-id');

            $.ajax({
                method: 'DELETE',
                url: '/comments/' + deleteId,
                dataType: 'json',
                data: {'post_id': postId},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response);
                    $('.comment_container_' + deleteId).remove();
                    $('body').alertPnotify({title: 'Comments', text: 'Comment deleted', type: 'error'});
                    $('.comments_count_' + response.post_id).text('(' + response.count + ')');
                },
                fail: function (jqXHR, textStatus, errorThrown) {
                }
            });
        }
    });

    $('body').on('click', '.update_comment', function () {
        var commentId = $(this).data('comment-id');
        $.ajax({
            method: 'PUT',
            url: '/comments/' + commentId,
            dataType: 'json',
            data: {'comment': $('#edit_comment_' + commentId).val()},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                //console.log(response);
                $('body').alertPnotify({title: 'Comment', text: 'You updated the comment', type: 'success'});
                $('.comment_body_' + commentId).text(response.text);
            },
            fail: function (jqXHR, textStatus, errorThrown) {
            }
        });
    });

});
