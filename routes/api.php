<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', ['uses' => 'ApiAuthController@authenticate']);
    Route::get('/logout', ['uses' => 'ApiAuthController@logout']);
    Route::post('/auth-status', ['uses' => 'ApiAuthController@authStatus']);
});

Route::group(['middleware' => ['auth.basic']], function () {
    Route::post('/upload-image ', 'Posts\PostsController@uploadImage');
    Route::post('/like-post', 'Posts\PostLikesController@likePost');
    Route::post('/get-post-users', 'Posts\PostLikesController@getPostUsers');
    Route::post('/get-comment-users', 'Posts\CommentsLikesController@getCommentUsers');
    Route::resource('posts', 'Posts\PostsController');
    //Route::get('posting', 'PostsController@test');
    Route::resource('comments', 'Posts\CommentsController');
    Route::resource('likes', 'Posts\PostLikesController');
    Route::resource('comments-likes', 'Posts\CommentsLikesController');
    Route::resource('users', 'Users\UsersController');

    //Change password
    Route::get('settings/change-password', 'Users\UsersController@changePassword');
    Route::put('settings/change-password', 'Users\UsersController@updatePassword');


    //Route::resource('admin', 'Admin\AdminController');
    //Route::resource('register', 'Admin\RegisterController');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', 'Admin\AdminController@index');

        Route::group(['prefix' => 'users'], function () {
            Route::get('add-new-user', 'Admin\Users\UsersController@addNewUserIndex');
            Route::get('view-users', 'Admin\Users\UsersController@viewUsers');
            Route::get('{id}/edit-user', 'Admin\Users\UsersController@edit');
            Route::put('{id}/update-user', 'Admin\Users\UsersController@update');
            Route::get('{id}/deactivate-user', 'Admin\Users\UsersController@deactivateUser');
            Route::get('{id}/activate-user', 'Admin\Users\UsersController@activateUser');
        });

    });
});
