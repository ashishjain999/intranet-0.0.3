<?php namespace App\Object;

use App;

/**
 * Class ObjectCreator
 * The Facade implementation
 *
 * @package App\Classes\Object
 */
class ObjectCreator
{

    /**
     * Creates a class object
     *
     * @param  $class
     * @return Object
     */
    public function object($class)
    {
        return App::make($class);
    }

}