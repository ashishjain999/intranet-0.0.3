<?php namespace App\Repositories\Contract;

/**
 * Interface RolesInterface
 *
 * @package App\Repositories\Contract
 */
interface RolesInterface extends RepositoryInterface
{

    /**
     * Get all the roles defined in a system
     *
     * @return mixed
     */
    public function getAllRoles();

}