<?php namespace App\Repositories\Contract;

/**
 * Interface PostsInterface
 *
 * @package App\Repositories\ContractZ
 */
interface CommentsInterface extends RepositoryInterface
{

    /**
     * @param array $postId
     * @return mixed
     */
    public function getAllComments(Array $postId);

    /**
     * Save comment to a database
     *
     * @param array $data
     * @return array
     */
    public function saveComment(Array $data);

    /**
     * Get count of comments
     *
     * @param $postId
     * @return mixed
     */
    public function getCommentsCount($postId);

    /**
     * Permanent delete of a comment
     *
     * @param $id
     * @return mixed
     */
    public function deleteAComment($id);

    /**
     * Update a comment
     *
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateAComment($data, $id);
}