<?php namespace App\Repositories\Contract;

/**
 * Interface RoleUserInterface
 *
 * @package App\Repositories\Contract
 */
interface RoleUserInterface extends RepositoryInterface
{

    /**
     * Update information of a user
     *
     * @param array $data
     * @return mixed
     */
    public function saveRoleUser(Array $data);

    /**
     * Update user's role
     *
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function updateRoleOfUser($data, $userId);

    /**
     * Get role of a user based on user_id
     *
     * @param $id
     * @return mixed
     */
    public function getRoleUser($id);

}