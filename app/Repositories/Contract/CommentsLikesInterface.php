<?php namespace App\Repositories\Contract;

/**
 * Interface PostsInterface
 *
 * @package App\Repositories\Contract
 */
interface CommentsLikesInterface extends RepositoryInterface
{

    /**
     * @param array $commentId
     * @return mixed
     */
    public function getAllCommentsLikes(Array $commentId);

    /**
     * Save comment to a database
     *
     * @param array $data
     * @return array
     */
    public function saveCommentLike(Array $data);

    /**
     * Get comment by userId & postId
     *
     * @param $userId
     * @param $commentId
     * @return mixed
     */
    public function getCommentsOnUserAndPost($userId, $commentId);

    /**
     * Delete a comment like
     *
     * @param $id
     * @return mixed
     */
    public function deleteLike($id);

    /**
     * Get count of likes for particular comment
     *
     * @param $commentId
     * @return mixed
     */
    public function getLikeCount($commentId);

    /**
     * Get all users who liked the comments
     *
     * @param $commentId
     * @return mixed
     */
    public function getCommentsUsers($commentId);
}