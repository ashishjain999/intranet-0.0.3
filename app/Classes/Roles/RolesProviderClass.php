<?php namespace App\Classes\Roles;

use App\Repositories\Contract\RolesInterface;

/**
 * Class RolesProviderClass
 *
 * @package App\Classes\Role
 */
class RolesProviderClass
{

    /**
     * @var RolesInterface
     */
    private $roles;

    /**
     * RolesProviderClass constructor.
     *
     * @param RolesInterface $roles
     */
    public function __construct(RolesInterface $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Get all the roles defined in a system
     *
     * @return mixed
     */
    public function getAllRoles()
    {
        return $this->roles->getAllRoles();
    }

}