<?php namespace App\Classes\RoleUser;

use App\Repositories\Contract\RoleUserInterface;

/**
 * Class RoleProviderClass
 *
 * @package App\Classes\RoleProvider
 */
class RoleUserProviderClass
{

    /**
     * @var RoleUserInterface
     */
    private $roleUser;

    /**
     * RoleProviderClass constructor.
     *
     * @param RoleUserInterface $roleUser
     */
    public function __construct(RoleUserInterface $roleUser)
    {
        $this->roleUser = $roleUser;
    }

    /**
     * Get role of a user based on user_id
     *
     * @param $id
     * @return mixed
     */
    public function getRoleUser($id)
    {
        return $this->roleUser->getRoleUser($id);
    }

    /**
     * Update user's role
     *
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function updateRoleOfUser($data, $userId)
    {
        return $this->roleUser->updateRoleOfUser($data, $userId);
    }
}