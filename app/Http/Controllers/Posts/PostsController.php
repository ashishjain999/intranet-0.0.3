<?php namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Facades\ObjectCreatorFacade;
use Auth;

/**
 * Class PostsController
 *
 * @package App\Http\Controllers\Posts
 */
class PostsController extends Controller
{

    /**
     * @var object
     */
    private $userConnect;
    private $postConnect;
    private $postLikeConnect;
    private $commentConnect;
    private $commentLikeConnect;

    /**
     * CommentsController constructor.
     * It will create a common variable for ObjectCreatorFacade
     */
    public function __construct()
    {
        $this->userConnect = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass');
        $this->postConnect = ObjectCreatorFacade::object('App\Classes\Posts\PostsProviderClass');
        $this->postLikeConnect = ObjectCreatorFacade::object('App\Classes\Posts\PostsLikesProviderClass');
        $this->commentConnect = ObjectCreatorFacade::object('App\Classes\Posts\CommentsProviderClass');
        $this->commentLikeConnect = ObjectCreatorFacade::object('App\Classes\Posts\CommentsLikesProvider');
    }

    /**
     * Display 10 posts on posts page.
     * Does pagination
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all user details & userId as a key
        $users = $this->userConnect->getAllUsers();

        //To get post with pagination
        $posts = $this->postConnect->getAllPostsByPagination($users);

        //To collect posts likes
        $likesRepo = $this->postLikeConnect->getLikesFromAllPosts($posts[ 'post_repo' ], $users, $posts[ 'post_id' ]);

        //To collect comments
        $comments = $this->commentConnect->getAllComments($posts[ 'post_id' ]);

        //To collect commentsLikes
        $commentLikes = $this->commentLikeConnect->getAllCommentsLikes($users, $comments[ 'comments' ],
            $comments[ 'comment_id' ]);

        $postsData = $this->postConnect->getEntirePosts($users, $posts, $comments[ 'comments' ]);

        $countPostRepo = $this->postConnect->getCountPostRepo();

        $paginate = new LengthAwarePaginator($postsData[ 'post_repo' ], $countPostRepo, config('config.pagination'), 0,
            [
                'path'  => request()->url(),
                'query' => request()->query()
            ]);

        //return view('posts.home', ['data' => $postsData[ 'post_repo' ], 'page' => $paginate]);

        return response()->json(['data' => $postsData[ 'post_repo' ], 'page' => $paginate]);
    }

    /**
     * Display the specified post.
     *
     * @param $sef
     * @return \Illuminate\Http\Response
     */
    public function show($sef)
    {
        //Get all user details & userId as a key
        //$users = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass')
        $users = $this->userConnect->getAllUsers();

        //Posts
        //$postsRepo = ObjectCreatorFacade::object('App\Classes\Posts\PostsProviderClass');
        $posts = $this->postConnect->getAPostBySEF($sef, $users);

        //PostLikes
        $postLikes = $this->postLikeConnect->getLikesForSinglePost($posts, $users, [$posts->id]);

        //Comments
        $comments = $this->commentConnect->getAllComments([$posts->id]);

        //CommentsLikes
        $commentLikes = $this->commentLikeConnect->getAllCommentsLikes($users, $comments[ 'comments' ],
            $comments[ 'comment_id' ]);

        $postData = $this->postConnect->getEntirePost($users, $posts, $comments[ 'comments' ]);

        //dd($postData); Show a view
        //return view('posts.one', ['post' => $postData]);
        return response()->json(['post' => $postData], 200);

    }

    /**
     * Store a newly created post in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response json
     */
    public function store(Request $request)
    {
        $data = $this->postConnect->store($request->all());

        return response()->json($data, 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int    $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $post = $this->postConnect->edit($request, $id);

        if (isset($post[ 'allowed' ])) {
            return view('posts.edit', ['post' => $post[ 'data' ]]);
        }

        if (Auth::id() !== $post[ 'data' ]->user_id) {
            abort(404);
        }

        return view('posts.edit', ['post' => $post[ 'data' ]]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->postConnect->update($request, $id);

        $request->session()->flash('alert', 'Post was successfully updated');

        return response()->redirectTo('/posts/' . $id . '/edit', 302);
    }

    /**
     * Upload image from CKEditor to storage
     *
     * @param Request $request
     * @return string
     */
    public function uploadImage(Request $request)
    {
        $image = $request->file('upload');
        $imageName = time() . $image->getClientOriginalName();

        $upload_success = $image->move(public_path('/public_images/' . Auth::id()), $imageName);

        $url = url('/public_images/' . Auth::id() . '/' . $imageName);

        return "<script>window.parent.CKEDITOR.tools.callFunction(1,'{$url}','')</script>";
    }

}
