<?php namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Facades\ObjectCreatorFacade;
use Auth;

/**
 * Class CommentsLikesController
 *
 * @package App\Http\Controllers\Posts
 */
class CommentsLikesController extends Controller
{

    /**
     * @var object
     */
    private $commentLikeConnect;
    private $userConnect;

    /**
     * CommentsController constructor.
     * It will create a common variable for ObjectCreatorFacade
     */
    public function __construct()
    {
        $this->commentLikeConnect = ObjectCreatorFacade::object('App\Classes\Posts\CommentsLikesProvider');
        $this->userConnect = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = $this->commentLikeConnect->getCommentsOnUserAndPost(Auth::id(), $request[ 'comment_id' ]);
        if ( !empty($query)) {

            //Delete entire record from Likes table
            $this->commentLikeConnect->deleteCommentLike($query->id);

            $count = $this->commentLikeConnect->getLikeCount($request[ 'comment_id' ]);
            $users = $this->commentLikeConnect->getCommentsUsers($request[ 'comment_id' ]);
            $single = 0;
            if (count($users) == 1) {
                $single = 1;
            }

            return response()->json(
                [
                    'comment_id' => $request[ 'comment_id' ],
                    'count'      => $count,
                    'msg'        => 'deleted',
                    'users'      => $users,
                    'single'     => $single
                ]
            );

        } else {
            $data = [
                'user_id'    => Auth::id(),
                'comment_id' => $request[ 'comment_id' ],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            //Saving likes
            $this->commentLikeConnect->saveCommentLike($data);

            $count = $this->commentLikeConnect->getLikeCount($request[ 'comment_id' ]);
            $users = $this->commentLikeConnect->getCommentsUsers($request[ 'comment_id' ]);
            $single = 0;
            if (count($users) == 1) {
                $single = 1;
            }

            return response()->json(
                [
                    'comment_id' => $data[ 'comment_id' ],
                    'count'      => $count,
                    'msg'        => 'success',
                    'users'      => $users,
                    'single'     => $single
                ]
            );

        }
    }

    /**
     * Returns the name, image of a person who liked the comments
     *
     * @param Request $request
     * @return array
     */
    public function getCommentUsers(Request $request)
    {
        return $this->commentLikeConnect->getCommentsUsers($request[ 'comment_id' ]);
    }
}
