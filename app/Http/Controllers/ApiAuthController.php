<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuthController extends Controller
{

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return response()->json(['status' => 'success', 'data' => Auth::user()]);
        }

        return response()->json(['status' => 'Invalid credentials', 'data' => null],
            401);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->session()->invalidate();

        Auth::logout();

        return response()->json(['status' => 'success', 'data' => null], 200);

    }

    /**
     * Check whether user is LoggedIn
     *
     * @return \Illuminate\Http\Response
     */
    public function authStatus()
    {
        if (Auth::check()) {
            return response(['authenticated' => true]);
        }

        return response(['authenticated' => false]);
    }
}