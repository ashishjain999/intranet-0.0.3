<?php namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Facades\ObjectCreatorFacade;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Admin\Users
 */
class UsersController extends Controller
{

    /**
     * @var object
     */
    private $userConnect;
    private $postConnect;
    private $rolesConnect;
    private $roleUserConnect;

    /**
     * UsersController constructor.
     * It will create a common variable for ObjectCreatorFacade
     */
    public function __construct()
    {
        $this->middleware('role:admin');
        $this->userConnect = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass');
        $this->postConnect = ObjectCreatorFacade::object('App\Classes\Posts\PostsProviderClass');
        $this->rolesConnect = ObjectCreatorFacade::object('App\Classes\Roles\RolesProviderClass');
        $this->roleUserConnect = ObjectCreatorFacade::object('App\Classes\RoleUser\RoleUserProviderClass');
    }

    /**
     * It will show the form to add a new user
     *
     * @return \Illuminate\Http\Response
     */
    public function addNewUserIndex()
    {
        return view('admin.users.add-new-user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewUsers()
    {
        //Get all users as per pagination
        $users = $this->userConnect->getAllUsersWithPagination(config('config.pagination'));

        return view('admin.users.view-users', ['users' => $users]);
    }

    /**
     * It will deactivate the user status
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function deactivateUser($id)
    {
        $this->userConnect->deactivateAUser($id);

        request()->session()->flash('alert', 'User was successfully deactivated');

        return back();
    }

    /**
     * It will activate the user status
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activateUser($id)
    {
        $this->userConnect->activateAUser($id);

        request()->session()->flash('alert', 'User was successfully activated');

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userConnect->getAUser($id);

        $unpublishedPosts = $this->postConnect->getUnpublishedPostsForAUser($id);

        $roles = $this->rolesConnect->getAllRoles();

        $roleUser = $this->roleUserConnect->getRoleUser($id);

        return view('admin.users.edit-user',
            ['user' => $user, 'roles' => $roles, 'role_user' => $roleUser, 'unpublished' => $unpublishedPosts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [
            'email'       => $request[ 'email' ],
            'name'        => $request[ 'name' ],
            'about_me'    => $request[ 'about_me' ],
            'team'        => $request[ 'team' ],
            'extn_no'     => $request[ 'extno' ],
            'designation' => $request[ 'designation' ],
            'mobile'      => $request[ 'mobile' ],
            'location'    => $request[ 'location' ],
            'birthday'    => date('Y-m-d 00:00:00', strtotime($request[ 'birthday' ])),
            'updated_at'  => date('Y-m-d H:i:s'),
        ];
        //dd($request->all());
        $this->userConnect->updateAUser($data, $id);

        if ( !empty($request[ 'role' ])) {
            $roleUser = [
                'role_id'    => $request[ 'role' ],
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->roleUserConnect->updateRoleOfUser($roleUser, $id);
        }

        $request->session()->flash('alert', 'User was successfully updated');

        return response()->redirectTo('/admin/users/' . $id . '/edit-user', 302);
    }
}
